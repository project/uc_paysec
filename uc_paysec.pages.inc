<?php
// $Id: uc_paysec.pages.inc,v 1.1.2.12 2010/08/11 17:52:20 islandusurper Exp $

/**
 * @file
 * Paysec administration menu items.
 */


// Handles a complete PaySec sale.
function uc_paysec_complete($order) {
  // If the order ID specified in the return URL is not the same as the one in
  // the user's session, we need to assume this is either a spoof or that the
  // user tried to adjust the order on this side while at PaySec. If it was a
  // legitimate checkout, the IPN will still come in from PaySec so the order
  // gets processed correctly. We'll leave an ambiguous message just in case.
  if (intval($_SESSION['cart_order']) != $order->order_id) {
    drupal_set_message(t('Thank you for your order! We will be notified by PaySec that we have received your payment.'));
    drupal_goto('cart');
  }
  // Ensure the payment method is PaySec.
  if ($order->payment_method != 'paysec') {
    drupal_goto('cart');
  }
  
  // Verifico transazione
  $result_code = uc_paysec_verify_payment($order);
  //uc_paysec_verify_payment($order, TRUE)
  switch ($result_code) {
    case 0:
      //SUCCESS
      watchdog('uc_paysec', 'PaySec transaction verified.');

      drupal_set_message(uc_paysec_message($result_code));
      
      //$_SESSION['do_complete'] = TRUE;
      //drupal_goto('cart/checkout/complete');
      drupal_goto();
      break;

    case 1:
      //REJECTED
      drupal_set_message(uc_paysec_message($result_code));
      //drupal_set_message('Your instruction to reject the payment has been performed successfully.');
      drupal_goto('cart');
      break;

    case 2:
      //LOGIN OR PASSWORD INVALID
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    case 3:
      //ACCOUNT BLOCKED OR CANCELLED
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    case 4:
      //TRANSACTION ID DOES NOT EXIST
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    case 6:
      //SYSTEM FAILURE	
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    case 5:
      //AMOUNT DIFFERENT FROM TRANSACTION
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    case 7:
      //PURCHASE ORDER NUMBER NOT ENTERED
      drupal_set_message(uc_paysec_message($result_code));
      drupal_goto('cart');
      break;

    default:
      drupal_set_message('Could not verify the payment status. We are working on fixing it.');
      drupal_goto('cart');
      break;
  }
}

// Handles a canceled Website Payments Standard sale.
function uc_paysec_cancel() {
  unset($_SESSION['cart_order']);
  drupal_set_message(t('Your instruction to reject the payment has been performed successfully.'));
  drupal_goto(variable_get('uc_paysec_cancel_return_url', 'cart'));
}

function uc_paysec_verify($order) {
  print t('Manual PaySec verification performed.');
  print '<pre>';
  print '<br /><br />';
  //var_dump($order);
  print '<br /><br />';
  $tmp = uc_paysec_verify_payment($order, TRUE);
  var_dump($tmp);
  print '</pre>';
  //drupal_set_message(t('Manual PaySec verification performed.'));
  //drupal_set_message(uc_paysec_admin_message(uc_paysec_verify_payment($order)));
  drupal_goto( 'admin/store/orders/' . $order->order_id);
}